﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This Server inheritated class acts like Server using UI elements like buttons and input fields.
/// </summary>
[SerializeField]
public class message
{
    public string Sender;
    public string Receiver;
    public string Message;
    public bool isEncrypt;
}
public class CustomServer : Server
{
    [Header("UI References")]
    [SerializeField] private Button m_StartServerButton = null;
    [SerializeField] private Button m_SendToClientButton = null;
    [SerializeField] private InputField m_SendToClientInputField = null;
    [SerializeField] private InputField m_IPServerInputField = null;
    //[SerializeField] private InputField m_PortServerInputField = null;
    [SerializeField] private Button m_CloseServerButton = null;
    [SerializeField] private Text m_ServerLogger = null;
    [SerializeField] private InputField m_ServerID = null;
    [SerializeField] private Toggle m_ServerIsEncrypt = null;
    [SerializeField] private Dropdown m_DropdownIpList = null;
    

    //[Header("Message")]
    //message messageInstance = new message();

    public void checkEncrypt()
    {
        if (m_ServerIsEncrypt.enabled)
        {
            messageInstanceServer.isEncrypt = true;
        }
        else
        {
            messageInstanceServer.isEncrypt = false;
        }
        
    }

    public void getIPFromInputField()
    {
        //ipAdress = m_IPServerInputField.text;
        int m_DropdownValue = m_DropdownIpList.value;
        //Change the message to say the name of the current Dropdown selection using the value
        ipAdress = m_DropdownIpList.options[m_DropdownValue].text;
        m_IPServerInputField.text = ipAdress;

        //port = int.Parse(m_PortServerInputField.text);
    }

    public void changeIPFromDropdown()
    {
        int m_DropdownValue = m_DropdownIpList.value;
        ipAdress = m_DropdownIpList.options[m_DropdownValue].text;
        m_IPServerInputField.text = ipAdress;
    }

    //Set UI interactable properties
    protected virtual void Awake()
    {
        //Start Server
        m_StartServerButton.interactable = true;  //Enable button to let users start the server
       
        m_StartServerButton.onClick.AddListener(StartServer);

        //Send to Client
        m_SendToClientButton.interactable = false;
        m_SendToClientButton.onClick.AddListener(SendMessageToClient);

        //Close Server
        m_CloseServerButton.interactable = false; //Disable button until the server is started
        m_CloseServerButton.onClick.AddListener(CloseServer);

        //Populate Server delegates
        OnClientConnected = () =>
        {
            m_SendToClientButton.interactable = true;
        };
    }

    //Start server and wait for clients
    protected override void StartServer()
    {
        string ID = SystemInfo.deviceUniqueIdentifier;
        IdServer = ID.Substring(0, 7);
        m_ServerID.text = "id:"+ IdServer;
        getIPFromInputField(); //ai
        base.StartServer();
        //Set UI interactable properties
        m_StartServerButton.interactable = false; //Disable button to avoid initilize more than one server
    }

    //Get input field text and send it to client
    private void SendMessageToClient()
    {
        string newMsg = m_SendToClientInputField.text;

        //to json pesan
        messageInstanceServer.Sender = IdServer;
        messageInstanceServer.Receiver = m_ServerReceiverInputField.text;
        messageInstanceServer.Message = newMsg;
        if (m_ServerIsEncrypt.isOn)
        {
            messageInstanceServer.isEncrypt = true;
        }
        else
        {
            messageInstanceServer.isEncrypt = false;
        }
        string messageToJason = JsonUtility.ToJson(messageInstanceServer);
        Debug.Log(messageToJason);
        //base.SendMessageToClient(newMsg);
        base.SendMessageToClient(messageToJason);
    }

    //Close connection with the client
    protected override void CloseClientConnection()
    {
        base.CloseClientConnection();
        //Set UI interactable properties
        m_CloseServerButton.interactable = true;  //Enable button to let users close the server
    }

    //Close client connection and disables the server
    protected override void CloseServer()
    {
        base.CloseServer();
        m_StartServerButton.interactable = true;
        m_CloseServerButton.interactable = false;
    }

    //Custom Server Log
    #region ServerLog
    //With Text Color
    protected override void ServerLog(string msg)
    {
        base.ServerLog(msg);
        m_ServerLogger.text += '\n' + "- " + msg;
    }
    //Without Text Color
    protected override void ServerLog(string msg, Color color)
    {
        base.ServerLog(msg, color);
        m_ServerLogger.text += '\n' + "<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">- " + msg + "</color>";
    }

    public void clearServerLog()
    {
        m_ServerLogger.text = "";
        m_ServerLogger.text = "Server Logs:";
    }
    #endregion
}