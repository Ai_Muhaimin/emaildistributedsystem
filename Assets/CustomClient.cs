﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This Client inheritated class acts like Client using UI elements like buttons and input fields.
/// </summary>
public class CustomClient : Client
{
    [Header("UI References")]
    [SerializeField] private Button m_StartClientButton = null;
    [SerializeField] private Button m_SendToServerButton = null;
    [SerializeField] private InputField m_SendToServerInputField = null;
    [SerializeField] private InputField m_IPClientInputField = null;
    //[SerializeField] private InputField m_PortClientInputField = null;
    [SerializeField] private Button m_SendCloseButton = null;
    [SerializeField] private Text m_ClientLogger = null;
    [SerializeField] private InputField m_ClientID = null;
    [SerializeField] private Toggle m_ClientIsEncrypt = null;
    [SerializeField] private InputField m_ClientReceiverInputField = null;
    [SerializeField] private Dropdown m_DropdownIpListClient = null;

    
    public void getIPFromInputField()
    {
        //int m_DropdownValue = m_DropdownIpListClient.value;
        //ipAddress = m_DropdownIpListClient.options[m_DropdownValue].text;
        ////port = int.Parse(m_PortClientInputField.text);
        //m_IPClientInputField.text = ipAddress;

        ipAddress = m_IPClientInputField.text;
    }



    public void changeIPFromDropdown()
    {
        int m_DropdownValue = m_DropdownIpListClient.value;
        ipAddress = m_DropdownIpListClient.options[m_DropdownValue].text;
        m_IPClientInputField.text = ipAddress;
    }

    //Set UI interactable properties
    private void Awake()
    {
        //Start Client
        
        m_StartClientButton.onClick.AddListener(StartClient);

        //Send to Server
        m_SendToServerButton.interactable = false;
        m_SendToServerButton.onClick.AddListener(SendMessageToServer);

        //SendClose
        m_SendCloseButton.interactable = false;
        m_SendCloseButton.onClick.AddListener(SendCloseToServer);

        //Populate Client delegates
        OnClientStarted = () =>
        {
            //Set UI interactable properties        
            m_SendCloseButton.interactable = true;
            m_SendToServerButton.interactable = true;
            m_StartClientButton.interactable = false;
        };

        OnClientClosed = () =>
        {
            //Set UI interactable properties        
            m_StartClientButton.interactable = true;
            m_SendToServerButton.interactable = false;
            m_SendCloseButton.interactable = false;
        };
    }

    //Start server and wait for clients
    protected void StartClient()
    {
        string ID = SystemInfo.deviceUniqueIdentifier;
        idClient = ID.Substring(0, 7);
        m_ClientID.text = "id:" + idClient;
        getIPFromInputField(); //ai
        base.StartClient();
       
    }

    private void SendMessageToServer()
    {
        string newMsg = m_SendToServerInputField.text;

        messageInstanceClient.Sender = idClient;
        messageInstanceClient.Receiver = m_ClientReceiverInputField.text;
        messageInstanceClient.Message = newMsg;
        if (m_ClientIsEncrypt.isOn)
        {
            messageInstanceClient.isEncrypt = true;
        }
        else
        {
            messageInstanceClient.isEncrypt = false;
        }
        string messageToJason = JsonUtility.ToJson(messageInstanceClient);
        Debug.Log(messageToJason);

        base.SendMessageToServer(messageToJason);
        //base.SendMessageToServer(newMsg);
    }

    private void SendCloseToServer()
    {
        base.SendMessageToServer("Close");
        //Set UI interactable properties        
        m_SendCloseButton.interactable = false;
    }

    //Custom Client Log
    #region ClientLog
    protected override void ClientLog(string msg, Color color)
    {
        base.ClientLog(msg, color);
        m_ClientLogger.text += '\n' + "<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">- " + msg + "</color>";
    }
    protected override void ClientLog(string msg)
    {
        base.ClientLog(msg);
        m_ClientLogger.text += '\n' + "- " + msg;
    }

    public void clearServerLog()
    {
        m_ClientLogger.text = "";
        m_ClientLogger.text = "Client Logs:";
    }
    #endregion
}