﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Net.NetworkInformation;

public class checkinternet : MonoBehaviour
{

    public CustomClient customClient;
    public CustomServer customServer;

    public Text inetInfo;
    public Image ImgSignInternet;
    string m_ReachabilityText;
    public int interval = 1;
    public float timeCheck = 0;
    public bool isCheckInternet = false;
    public bool isConnect = false;

    float timer;
    //public string ip;
    //public string ip2;
    public Dropdown m_dropdownIPServer;
    public Dropdown m_dropdownIPClient;
    public List<string> listIP = new List<string>();

    public void addlistIP(bool isServer)
    {
       
        m_dropdownIPServer.ClearOptions();
        m_dropdownIPServer.options.Clear();

        m_dropdownIPClient.ClearOptions();
        m_dropdownIPClient.options.Clear();
        listIP.Clear();

        //ip = IPManager.GetIP(ADDRESSFAM.IPv4);

        foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
            //if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                {
                foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        //do what you want with the IP here... add it to a list, just get the first and break out. Whatever.
                        //Debug.Log(ip.Address.ToString());
                        listIP.Add(ip.Address.ToString());
                    }
                }
            }
        }

        if (isServer)
        {
            m_dropdownIPServer.AddOptions(listIP);
            customServer.getIPFromInputField();
        }
        else
        {
            m_dropdownIPClient.AddOptions(listIP);
            customClient.getIPFromInputField();
        }

       
    }
    void Update()
    {
        if (isCheckInternet)
        {

            timeCheck += Time.deltaTime;
            if (timeCheck > interval)
            {
                isCheckInternet = false;
                timeCheck = 0;
                CheckInternetReachable();
                isCheckInternet = true;
            }
        }
    }

    public void CheckInternetReachable()
    {
        //Output the network reachability to the console window
        Debug.Log("Internet : " + m_ReachabilityText);
        //Check if the device cannot reach the internet
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //Change the Text
            m_ReachabilityText = "Not Reachable.";
            ImgSignInternet.color = Color.red;
            isConnect = false;
        }
        //Check if the device can reach the internet via a carrier data network
        else if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
        {
            m_ReachabilityText = "Reachable via carrier data network.";
            ImgSignInternet.color = Color.green;
            isConnect = true;
        }
        //Check if the device can reach the internet via a LAN
        else if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            m_ReachabilityText = "Reachable via Local Area Network.";
            ImgSignInternet.color = Color.green;
            isConnect = true;
        }

        isCheckInternet = true;
        interval = 5;
    }

    
}
