﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject Server;
    public GameObject Client;
    public checkinternet internetManager;


    public void resetapp()
    {
        SceneManager.LoadScene(0);
    }

    public void showMenu(string menu)
    {
        if(menu == "menu")
        {
            MainMenu.transform.localScale = Vector3.one;
            Client.transform.localScale = Vector3.zero;
            Server.transform.localScale = Vector3.zero;
            internetManager.addlistIP(false);
        }
        else if (menu == "server")
        {
            MainMenu.transform.localScale = Vector3.zero;
            Client.transform.localScale = Vector3.zero;
            Server.transform.localScale = Vector3.one;
            internetManager.addlistIP(true);

        }
        else if (menu == "client")
        {
            MainMenu.transform.localScale = Vector3.zero;
            Client.transform.localScale = Vector3.one;
            Server.transform.localScale = Vector3.zero;
            internetManager.addlistIP(false);
        }
    }

    
}
